<?php

    $hero = get_field('hero');
    $headline = $hero['headline'];
    $deck = $hero['deck'];
    $photo = $hero['photo'];


?>

<section class="hero grid">

    <div class="info">
        <div class="headline">
            <h1 class="page-title"><?php echo $headline; ?></h1>
        </div>
    </div>

    <div class="photo">
        <div class="content">
            <?php echo wp_get_attachment_image($photo['ID'], 'full'); ?>
        </div>
    </div>

    <div class="ornament">
        <img src="<?php bloginfo('template_directory'); ?>/images/bg-circle-dots-blue.svg" alt="Circle Dots" />
    </div>

</section>