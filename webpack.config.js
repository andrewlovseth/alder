const path = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const BrowserSyncPlugin = require('browser-sync-webpack-plugin');

module.exports = {
    mode: "development",
    context: path.resolve(__dirname, "assets"),
    output: {
        filename: 'main.js',
        path: path.resolve(__dirname, "assets/dist")
    },
    plugins: [
        new MiniCssExtractPlugin({ filename: "[name].css" }),
        new BrowserSyncPlugin(
            {
              proxy: 'https://alder.local/',
              open: false,
              injectChanges: true,
              files: [
                {
                  match: ['assets/dist/*.css', 'assets/dist/*.js', '**/*.php'],
                  fn: (event, file) => {
                    if ('change' == event) {
                      const bs = require('browser-sync').get('bs-webpack-plugin');
      
                      let filename = file.substring(file.lastIndexOf('/') + 1);
                      console.log(`filename ${filename}`);
      
                      if (filename == 'style.js' || filename == 'style.css') {
                        bs.reload('*.css');
                      } else if (
                        file.split('.').pop() == 'js' ||
                        file.split('.').pop() == 'php'
                      ) {
                        bs.reload();
                      }
                    }
                  },
                },
              ],
            },
        )
    ],
    watch: true,
    module: {
        rules: [
            {
                test: /\.scss$/,
                use: [
                    MiniCssExtractPlugin.loader,
                    'css-loader',
                    'sass-loader'
                ]
            }
        ]
    }
}