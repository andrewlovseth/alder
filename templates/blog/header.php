<?php

    $home_id = get_option('page_on_front');
    $header = get_field('section_header', $home_id);
    $header_photo = $header['photo'];
    $header_sub_header = $header['sub_header'];
    $header_signature = $header['signature'];
    $header_name = $header['name'];
    $header_meta = $header['meta'];



?>

<section class="blog blog-index-header grid">
    <div class="section-header">
        <div class="sub-headline">
            <h5 class="sub-header blue"><?php echo $header_sub_header; ?></h5>
        </div>

        <div class="photo">
            <?php echo wp_get_attachment_image($header_photo['ID'], 'full'); ?>
        </div>

        <div class="info">
            <div class="signature">
                <?php echo wp_get_attachment_image($header_signature['ID'], 'full'); ?>
            </div>

            <h3 class="name"><?php echo $header_name; ?></h3>
            <h4 class="meta"><?php echo $header_meta; ?></h4>
        </div>
    </div>
</section>