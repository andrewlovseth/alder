<section class="posts grid">
    <?php if ( have_posts() ): while ( have_posts() ): the_post(); ?>

        <article <?php ?>>
            <div class="photo">
                <div class="content">
                    <?php the_post_thumbnail('large'); ?>
                </div>
            </div>

            <div class="info">
                <div class="date">
                    <h5 class="sub-header gray-blue"><?php the_time('F j, Y'); ?></h5>
                </div>

                <div class="headline">
                    <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
                </div>

                <div class="excerpt copy-3">
                    <p><?php the_excerpt(); ?></p>
                </div>

                <div class="cta">
                    <a class="btn" href="<?php the_permalink(); ?>">Read More</a>
                </div>
            </div>

        </article>

    <?php endwhile; endif; ?>
</section>

