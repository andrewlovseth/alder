<article class="post grid">
    <?php if ( have_posts() ): while ( have_posts() ): the_post(); ?>

        <section class="article-header">
            <div class="featured-photo">
                <div class="content">
                    <?php the_post_thumbnail(); ?>
                </div>
            </div>

            <div class="date">
                <h5 class="sub-header gray-blue"><?php the_time('F j, Y'); ?></h5>
            </div>

            <div class="headline">
                <h1 class="section-title"><?php the_title(); ?></h1>
            </div>
        </section>

        <section class="article-body copy copy-2 extended">
            <p><?php the_content(); ?></p>
        </section>

    <?php endwhile; endif; ?>
</article>