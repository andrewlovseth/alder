<?php

$ceo = get_field('ceo');
$headline = $ceo['headline'];
$copy = $ceo['copy'];
$photo = $ceo['photo'];

?>

<section class="ceo grid">
    
    <div class="photo">
        <?php echo wp_get_attachment_image($photo['ID'], 'full'); ?>
    </div>

    <div class="info">
        <div class="headline">
            <h3 class="section-title"><?php echo $headline; ?></h3>
        </div>

        <div class="copy copy-3 extended">
            <?php echo $copy; ?>
        </div>
    </div>

</section>