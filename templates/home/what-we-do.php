<?php

    $what = get_field('what_we_do');
    $sub_headline = $what['sub_headline'];
    $headline = $what['headline'];
    $copy = $what['copy'];
    $link = $what['link'];
    $photo = $what['photo'];

?>

<section class="what-we-do grid">
    <div class="info">

        <div class="sub-headline">
            <h5 class="sub-header"><?php echo $sub_headline; ?></h5>
        </div>

        <div class="headline">
            <h2 class="section-title"><?php echo $headline; ?></h2>
        </div>

        <div class="copy copy-2">
            <?php echo $copy; ?>
        </div>

        <?php 
            if( $link ): 
            $link_url = $link['url'];
            $link_title = $link['title'];
            $link_target = $link['target'] ? $link['target'] : '_self';
        ?>

            <div class="cta">
                <a class="btn" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
            </div>

        <?php endif; ?>

    </div>

    <div class="photo">
        <?php echo wp_get_attachment_image($photo['ID'], 'full'); ?>
    </div>
</section>