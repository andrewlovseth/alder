<?php

    $header = get_field('section_header');
    $header_photo = $header['photo'];
    $header_sub_header = $header['sub_header'];
    $header_signature = $header['signature'];
    $header_name = $header['name'];
    $header_meta = $header['meta'];

    $blog = get_field('blog');
    $blog_post = $blog['post'];
    $blog_photo = $blog['photo'];
    $blog_excerpt = $blog['excerpt'];

?>

<section class="blog grid">
    <div class="section-header">
        <div class="sub-headline">
            <h5 class="sub-header blue"><?php echo $header_sub_header; ?></h5>
        </div>

        <div class="photo">
            <?php echo wp_get_attachment_image($header_photo['ID'], 'full'); ?>
        </div>

        <div class="info">
            <div class="signature">
                <?php echo wp_get_attachment_image($header_signature['ID'], 'full'); ?>
            </div>

            <h3 class="name"><?php echo $header_name; ?></h3>
            <h4 class="meta"><?php echo $header_meta; ?></h4>
        </div>
    </div>

    <div class="post-photo">
        <a href="<?php echo get_permalink($blog_post->ID); ?>">
            <?php if($blog_photo): ?>
                <?php echo wp_get_attachment_image($blog_photo['ID'], 'full'); ?>
            <?php else: ?>
                <?php echo get_the_post_thumbnail($blog_post->ID, 'large'); ?>
            <?php endif; ?>
        </a>

        <div class="ornament">
            <img src="<?php bloginfo('template_directory'); ?>/images/bg-circle-dots-gold.svg" alt="Circle Dots" />
        </div>
    </div>

    <div class="post-info">
        <div class="date">
            <h5 class="sub-header gray-blue"><?php echo get_the_time('F j, Y', $blog_post->ID); ?></h5>
        </div>

        <div class="headline">
            <h2>
                <a href="<?php echo get_permalink($blog_post->ID); ?>">
                    <?php echo get_the_title($blog_post->ID); ?>
                </a>
            </h2>
        </div>

        <div class="excerpt copy-3">
            <?php if($blog_excerpt): ?>
                <?php echo $blog_excerpt; ?>
            <?php else: ?>
                <p><?php echo get_the_excerpt($blog_post->ID); ?></p>
            <?php endif; ?>
        </div>

        <div class="cta">
            <a class="btn" href="<?php echo get_permalink($blog_post->ID); ?>">Read More</a>
        </div>
    </div>    
</section>