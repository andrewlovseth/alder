<?php

    $hero = get_field('hero');
    $photo = $hero['photo'];
    $headline = $hero['headline'];
    $deck = $hero['deck'];
    $link = $hero['link'];

?>

<section class="home-hero grid">
    <div class="info">
        <div class="info-wrapper">

            <div class="headline">
                <h1 class="hero-title"><?php echo $headline; ?></h1>
            </div>

            <div class="copy copy-1">
                <p><?php echo $deck; ?></p>
            </div>

            <?php 
                if( $link ): 
                $link_url = $link['url'];
                $link_title = $link['title'];
                $link_target = $link['target'] ? $link['target'] : '_self';
            ?>

                <div class="cta">
                    <a class="btn" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
                </div>

            <?php endif; ?>

        </div>
    </div>

    <div class="photo">
        <div class="content">
            <?php echo wp_get_attachment_image($photo['ID'], 'full'); ?>
        </div>
    </div>

    <div class="ornament">
        <img src="<?php bloginfo('template_directory'); ?>/images/bg-circle-dots-blue.svg" alt="Circle Dots" />
    </div>
</section>