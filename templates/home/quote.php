<?php

    $args = wp_parse_args($args);

    if(!empty($args)) {
        $count = $args['count'];
        $repeater = $args['repeater'];
        $rand = $args['rand'];
    }

    $photo = get_sub_field('photo');
    $sub_headline = get_sub_field('sub_headline');

    $text = $repeater[$rand]['text'];
    $name = $repeater[$rand]['name'];
    $meta = $repeater[$rand]['meta'];

?>

<section class="quote quote-<?php echo $count; ?> grid">
    <div class="info">
        <div class="sub-headline">
            <h5 class="sub-header gray-blue"><?php echo $sub_headline; ?></h5>
        </div>

        <blockquote>
            <?php echo $text; ?>
        </blockquote>

        <div class="source">
            <div class="source__info">
                <h4 class="name"><?php echo $name; ?></h4>
                <h5 class="meta"><?php echo $meta; ?></h5>
            </div>
        </div>
    </div>

    <div class="photo">
        <?php echo wp_get_attachment_image($photo['ID'], 'full'); ?>
        
        <?php if($photo['caption']): ?>
            <div class="caption">
                <p><?php echo $photo['caption']; ?></p>
            </div>

        <?php endif; ?>

        <div class="photo-ornament"></div>
    </div>

    <div class="section-ornament"></div>
</section>