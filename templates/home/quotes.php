<?php if(have_rows('quotes')): $count = 1;  while(have_rows('quotes')) : the_row(); ?>

	<?php if( get_row_layout() == 'quote' ): ?>

        <?php
            $repeater = get_sub_field('quote_set');
            $rand = rand(0, (count($repeater) - 1));

            $args = [
                'count' => $count,
                'repeater' => $repeater,
                'rand' => $rand
            ];
            get_template_part('templates/home/quote', null, $args);
        ?>

	<?php endif; ?>

<?php $count++;  endwhile; endif; ?>