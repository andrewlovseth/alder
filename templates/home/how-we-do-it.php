<?php

    $how = get_field('how_we_do_it');
    $sub_headline = $how['sub_headline'];
    $headline = $how['headline'];
    $photo = $how['photo'];
    $copy = $how['copy'];
    $link = $how['link'];
    

?>

<section class="how-we-do-it grid">
    <div class="ornament">
        <img src="<?php bloginfo('template_directory'); ?>/images/bg-topo-gray.svg" alt="Topo Map" />
    </div>
    
    <div class="sub-headline">
        <h5 class="sub-header blue"><?php echo $sub_headline; ?></h5>
    </div>

    <div class="headline">
        <h2 class="section-title gold"><?php echo $headline; ?></h2>
    </div>


    <div class="photo">
        <?php echo wp_get_attachment_image($photo['ID'], 'full'); ?>
    </div>

    <div class="copy copy-2">
        <?php echo $copy; ?>
    </div>

    <?php 
        if( $link ): 
        $link_url = $link['url'];
        $link_title = $link['title'];
        $link_target = $link['target'] ? $link['target'] : '_self';
    ?>

        <div class="cta">
            <a class="btn" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
        </div>

    <?php endif; ?>

</section>