<?php if(have_rows('faqs')): ?>

    <section class="faqs grid">

        <?php $count = 1; while(have_rows('faqs')) : the_row(); ?>

            <?php if( get_row_layout() == 'faq' ): ?>

                <div class="faq<?php if($count == 1):?> active<?php endif; ?>">
                    <div class="question">
                        <h3 class="section-title small"><?php the_sub_field('question'); ?></h3>
                    </div>

                    <div class="answer copy copy-3 extended">
                        <?php the_sub_field('answer'); ?>
                    </div>
                </div>

            <?php endif; ?>

        <?php $count++; endwhile; ?>

    </section>

<?php endif; ?>