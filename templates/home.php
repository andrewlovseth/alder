<?php
/*

    Template Name: Home

*/

get_header(); ?>

    <?php get_template_part('templates/home/hero'); ?>

    <?php get_template_part('templates/home/what-we-do'); ?>

    <?php get_template_part('templates/home/how-we-do-it'); ?>

    <?php get_template_part('templates/home/quotes'); ?>

    <?php get_template_part('templates/home/blog'); ?>

<?php get_footer(); ?>