<?php
/*

    Template Name: FAQs

*/

get_header(); ?>

    <?php get_template_part('template-parts/global/hero'); ?>

    <?php get_template_part('templates/faqs/faqs'); ?>

<?php get_footer(); ?>