	</main> <!-- .site-content -->

	<footer class="site-footer grid">
		<?php get_template_part('template-parts/footer/membership-banner'); ?>

		<div class="footer-nav">
			<?php get_template_part('template-parts/footer/logo'); ?>

			<div class="footer-links">
				<?php get_template_part('template-parts/header/nav-links'); ?>
			</div>

			<?php get_template_part('template-parts/footer/social-links'); ?>

			<?php get_template_part('template-parts/footer/contact-info'); ?>

			<?php get_template_part('template-parts/footer/copyright'); ?>
		</div>

		<?php get_template_part('template-parts/footer/quotes'); ?>
	</footer>

<?php wp_footer(); ?>

</div> <!-- .site -->

</body>
</html>